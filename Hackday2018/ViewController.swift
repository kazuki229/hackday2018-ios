/*
 Copyright (C) 2016 Apple Inc. All Rights Reserved.
 See LICENSE.txt for this sample’s licensing information

 Abstract:
 The primary view controller. The speach-to-text engine is managed an configured here.
 */

import UIKit
import Speech
import AVKit

public class ViewController: UIViewController, SFSpeechRecognizerDelegate {
    // MARK: Properties

    private let speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "ja-JP"))!

    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?

    private var recognitionTask: SFSpeechRecognitionTask?

    private let audioEngine = AVAudioEngine()

    private var str: String = ""

    private var wordView: [UILabel] = []

    private var animationCount: Int = 0

    private var audioPlayer: AVAudioPlayer!

    private let wordSize: CGFloat = 50

    private var godImageView: UIImageView!

    private var timer: Timer!

    private var isFlushing = false

//    @IBOutlet var textView : UITextView!

    private var recordButton : UIButton!

    // MARK: UIViewController

    public override func viewDidLoad() {
        super.viewDidLoad()

        // Disable the record buttons until authorization has been granted.
        recordButton = UIButton(frame: CGRect(x: UIScreen.main.bounds.size.width - 180, y: UIScreen.main.bounds.size.height - 50, width: 150, height: 40))
        recordButton.setTitle("Start Recording", for: .normal)
        recordButton.backgroundColor = UIColor.red
        recordButton.addTarget(self, action: #selector(recordButtonTapped), for: .touchUpInside)

        godImageView = UIImageView(image: UIImage(named: "god1"))
        godImageView.translatesAutoresizingMaskIntoConstraints = false
        godImageView.alpha = 0.0

        view.addSubview(godImageView)
        view.addSubview(recordButton)

        godImageView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        godImageView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
    }

    override public func viewDidAppear(_ animated: Bool) {
        speechRecognizer.delegate = self

        SFSpeechRecognizer.requestAuthorization { authStatus in
            /*
             The callback may not be called on the main thread. Add an
             operation to the main queue to update the record button's state.
             */
            OperationQueue.main.addOperation {
                switch authStatus {
                case .authorized:
                    self.recordButton.isEnabled = true

                case .denied:
                    self.recordButton.isEnabled = false
                    self.recordButton.setTitle("User denied access to speech recognition", for: .disabled)

                case .restricted:
                    self.recordButton.isEnabled = false
                    self.recordButton.setTitle("Speech recognition restricted on this device", for: .disabled)

                case .notDetermined:
                    self.recordButton.isEnabled = false
                    self.recordButton.setTitle("Speech recognition not yet authorized", for: .disabled)
                }
            }
        }
    }

    private func startRecording() throws {

        // Cancel the previous task if it's running.
        if let recognitionTask = recognitionTask {
            recognitionTask.cancel()
            self.recognitionTask = nil
        }

        let audioSession = AVAudioSession.sharedInstance()
        try audioSession.setCategory(.record, mode: .measurement, options: [])
        try audioSession.setMode(.measurement)
        try audioSession.setActive(true, options: .notifyOthersOnDeactivation)

        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()

        let inputNode = audioEngine.inputNode
        guard let recognitionRequest = recognitionRequest else { fatalError("Unable to created a SFSpeechAudioBufferRecognitionRequest object") }

        // Configure request so that results are returned before audio recording is finished
        recognitionRequest.shouldReportPartialResults = true

        // A recognition task represents a speech recognition session.
        // We keep a reference to the task so that it can be cancelled.
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest) { result, error in
            var isFinal = false

            if let result = result {
                if result.bestTranscription.formattedString.count >= self.str.count {
                    print("result = \(result.bestTranscription.formattedString)")
                    print("self.str = \(self.str)")
                    if (self.str.count > 0 && self.isFlushing == false) {
                        if self.timer == nil || self.timer.isValid == false {
                            self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.inputRequest), userInfo: nil, repeats: true)
                        }
                    }
                    // 差分の文字列取得
                    let diff = String(result.bestTranscription.formattedString.suffix(result.bestTranscription.formattedString.count - self.str.count))
                    // 位置文字ずつ描画する
                    diff.forEach {
                        let label = UILabel()
                        label.text = String($0)
                        label.font = UIFont.init(name: "Arial", size: 36)
                        let position = self.getWordPosition(count: self.wordView.count + 1)
                        label.frame = CGRect(x: position.x, y: position.y, width: self.wordSize, height: self.wordSize)
                        label.backgroundColor = UIColor.yellow
                        self.view.addSubview(label)
                        self.wordView.append(label)
                    }
                    self.str = result.bestTranscription.formattedString
                }
                isFinal = result.isFinal
            }

            if error != nil || isFinal {
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)

                self.recognitionRequest = nil
                self.recognitionTask = nil

                self.recordButton.isEnabled = true
                self.recordButton.setTitle("Start Recording", for: [])
            }
        }

        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
            self.recognitionRequest?.append(buffer)
        }
        audioEngine.prepare()
        try audioEngine.start()
    }

    // MARK: SFSpeechRecognizerDelegate
    public func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            recordButton.isEnabled = true
            recordButton.setTitle("Start Recording", for: [])
        } else {
            recordButton.isEnabled = false
            recordButton.setTitle("Recognition not available", for: .disabled)
        }
    }

    // MARK: Interface Builder actions
    @IBAction func recordButtonTapped() {
        if audioEngine.isRunning {
            audioEngine.stop()
            recognitionRequest?.endAudio()
            recordButton.isEnabled = false
            recordButton.setTitle("Stopping", for: .disabled)
            //timerが動いてるなら.
            if self.timer != nil && self.timer.isValid == true {
                //timerを破棄する.
                self.timer.invalidate()
                self.timer = nil
            }
            self.fetchScore()
            flush()
        } else {
            try! startRecording()
            recordButton.setTitle("Stop recording", for: [])
        }
    }

    private func flush() {
        isFlushing = true
        self.playSound()
        // CAKeyframeAnimationオブジェクトを生成
        for i in 0..<self.wordView.count {
            let animation = CAKeyframeAnimation(keyPath: "position")
            animation.fillMode = .forwards
            animation.isRemovedOnCompletion = false
            animation.duration = 4.0
            animation.beginTime = CACurrentMediaTime() + Double(i) * 0.1

            let scaleAnim = CAKeyframeAnimation(keyPath: "transform.scale")
            scaleAnim.values = [1, 0]
            scaleAnim.duration = 4.0
            scaleAnim.fillMode = .forwards
            scaleAnim.isRemovedOnCompletion = false
            scaleAnim.beginTime = CACurrentMediaTime() + Double(i) * 0.1

            // 放物線のパスを生成
            let startPos = CGPoint(x: 0, y: 0)
            let path = CGMutablePath()
            let endPoint = CGPoint(x: UIScreen.main.bounds.size.width/2, y: UIScreen.main.bounds.size.height/2)
            let size = UIScreen.main.bounds.width/2 - CGFloat(20)
            let diff = size/12

            let randConst: CGFloat = 20

            let position: [CGPoint] = [
                // 初期値
                self.wordView[i].frame.origin,
                // 1回転目
                CGPoint(x:endPoint.x + size + CGFloat.random(in: -randConst..<randConst), y:endPoint.y + CGFloat.random(in: -randConst..<randConst)),
                CGPoint(x:endPoint.x + CGFloat.random(in: -randConst..<randConst), y:endPoint.y + size - diff + CGFloat.random(in: -randConst..<randConst)),
                CGPoint(x:endPoint.x - (size - diff*2) + CGFloat.random(in: -randConst..<randConst), y:endPoint.y + CGFloat.random(in: -randConst..<randConst)),
                CGPoint(x:endPoint.x + CGFloat.random(in: -randConst..<randConst), y:endPoint.y - (size - diff*3) + CGFloat.random(in: -randConst..<randConst)),
                // 2回転目
                CGPoint(x:endPoint.x + size - diff*4 + CGFloat.random(in: -randConst..<randConst), y:endPoint.y + CGFloat.random(in: -randConst..<randConst)),
                CGPoint(x:endPoint.x + CGFloat.random(in: -randConst..<randConst), y:endPoint.y + size - diff*5 + CGFloat.random(in: -randConst..<randConst)),
                CGPoint(x:endPoint.x - (size - diff*6) + CGFloat.random(in: -randConst..<randConst), y:endPoint.y + CGFloat.random(in: -randConst..<randConst)),
                CGPoint(x:endPoint.x + CGFloat.random(in: -randConst..<randConst), y:endPoint.y - (size - diff*7) + CGFloat.random(in: -randConst..<randConst)),
                // 3回転目
                CGPoint(x:endPoint.x + size - diff*8 + CGFloat.random(in: -randConst..<randConst), y:endPoint.y + CGFloat.random(in: -randConst..<randConst)),
                CGPoint(x:endPoint.x + CGFloat.random(in: -randConst..<randConst), y:endPoint.y + size - diff*9 + CGFloat.random(in: -randConst..<randConst)),
                CGPoint(x:endPoint.x - (size - diff*10) + CGFloat.random(in: -randConst..<randConst), y:endPoint.y + CGFloat.random(in: -randConst..<randConst)),
                CGPoint(x:endPoint.x + CGFloat.random(in: -randConst..<randConst) , y:endPoint.y - (size - diff*11) + CGFloat.random(in: -randConst..<randConst)),
                ]
            path.move(to: startPos)

            for i in 1..<position.count {
                let base: CGFloat = UIScreen.main.bounds.size.width/4 // 100
                let bezie = CGFloat(base / CGFloat(12) * CGFloat(position.count - i))

                switch i % 4 {
                case 1:
                    path.addCurve(to: position[i], control1: CGPoint(x: position[i-1].x + bezie, y: position[i-1].y), control2: CGPoint(x: position[i].x, y: position[i].y - bezie))
                case 2:
                    path.addCurve(to: position[i], control1: CGPoint(x: position[i-1].x, y: position[i-1].y + bezie), control2: CGPoint(x: position[i].x + bezie, y: position[i].y))
                case 3:
                    path.addCurve(to: position[i], control1: CGPoint(x: position[i-1].x - bezie, y: position[i-1].y), control2: CGPoint(x: position[i].x, y: position[i].y + bezie))
                case 0:
                    path.addCurve(to: position[i], control1: CGPoint(x: position[i-1].x, y: position[i-1].y - bezie), control2: CGPoint(x: position[i].x - bezie, y: position[i].y))
                default:
                    break
                }
            }
            animation.path = path
            animation.delegate = self

            // レイヤーにアニメーションを追加
            self.wordView[i].layer.add(animation, forKey: nil)
            self.wordView[i].layer.add(scaleAnim, forKey: nil)
        }
    }

    private func getWordPosition(count: Int) -> CGPoint {
        // 横並びにできるMax値
        let maxX = Int(UIScreen.main.bounds.size.width / wordSize)

        let x = CGFloat(count % maxX) * wordSize
        let y = CGFloat(count / maxX) * self.wordSize
        return CGPoint(x: x, y: y+30)
    }

    private func finishRequst() {
        let url = URL(string: "http://52.194.219.176:4567/effect/do")
        let request = URLRequest(url: url!)
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if error == nil, let data = data, let response = response as? HTTPURLResponse {
                // HTTPヘッダの取得
//                print("Content-Type: \(response.allHeaderFields["Content-Type"] ?? "")")
                // HTTPステータスコード
//                print("statusCode: \(response.statusCode)")
//                print(String(data: data, encoding: String.Encoding.utf8) ?? "")
            }
        }.resume()
    }

    @objc private func inputRequest() {
        let url = URL(string: "http://52.194.219.176:4567/wash")
        let request = URLRequest(url: url!)
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if error == nil, let data = data, let response = response as? HTTPURLResponse {
                // HTTPヘッダの取得
//                print("Content-Type: \(response.allHeaderFields["Content-Type"] ?? "")")
                // HTTPステータスコード
//                print("statusCode: \(response.statusCode)")
                print(String(data: data, encoding: String.Encoding.utf8) ?? "")
                let washResponse = try! JSONDecoder().decode(WashResponse.self, from: data)
                if washResponse.wash {
                    print("wash!!")
                    if Thread.isMainThread {
                        self.recordButtonTapped()
                    } else {
                        DispatchQueue.main.async {
                            self.recordButtonTapped()
                        }
                    }
                } else {
//                    print("not wash")
                }
            }
        }.resume()
    }

    private func fetchScore() {
        let urlString: String = "http://52.194.219.176:4567/score?text=\(urlEncode(string: self.str))"
        let url = URL(string: urlString)!

        print("url = \(url.absoluteString)")
        let request = URLRequest(url: url)
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if error == nil, let data = data, let response = response as? HTTPURLResponse {
                let scoreResponse = try! JSONDecoder().decode(ScoreResponse.self, from: data)
                if scoreResponse.score < 0 {
                    self.showGod()
                    print("神")
                } else {
                    self.showGod()
                    print("普通")
                }
            }
        }.resume()
    }

    private func showGod() {
        let random = Int.random(in: 1...10)
        let name: String = "god\(random)"
        print("name = \(name)")
        if Thread.isMainThread {
            self.godImageView.image = UIImage(named: name)
            UIView.animate(withDuration: 3) {
                self.godImageView.alpha = 1.0
            }
        } else {
            DispatchQueue.main.async {
                self.godImageView.image = UIImage(named: name)
                UIView.animate(withDuration: 3) {
                    self.godImageView.alpha = 1.0
                }
            }
        }
    }

    private func hideGod() {
        if Thread.isMainThread {
            UIView.animate(withDuration: 0.5) {
                self.godImageView.alpha = 0.0
            }
        } else {
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.5) {
                    self.godImageView.alpha = 0.0
                }
            }
        }
    }

    private func urlEncode(string: String) -> String {
        return string.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
    }
}
struct ScoreResponse: Codable {
    let score: Float
}
struct WashResponse: Codable {
    let wash: Bool
}

extension ViewController : CAAnimationDelegate {

    public func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if animationCount + 1 == self.wordView.count {
            // 最後の1つの場合はcountをリセット
            finishRequst()
            self.str = ""
            self.wordView.forEach {
                $0.removeFromSuperview()
            }

            self.wordView = []
            animationCount = 0

            // ボタンを初期化
            if Thread.isMainThread {
                self.hideGod()
                self.recordButtonTapped()
            } else {
                DispatchQueue.main.async {
                    self.hideGod()
                    self.recordButtonTapped()
                }
            }
            isFlushing = false
        } else {
            // それ以外はカウントを増やす
            animationCount += 1
        }
    }
}

extension ViewController: AVAudioPlayerDelegate {
    func playSound() {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(.playback, mode: .default, options: [])
            try audioSession.setMode(.default)
            try audioSession.setActive(false, options: .notifyOthersOnDeactivation)
        } catch {

        }
        guard let path = Bundle.main.path(forResource: "flush", ofType: "mp3") else {
            print("音源ファイルが見つかりません")
            return
        }

        do {
            // AVAudioPlayerのインスタンス化
            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: path))
            // AVAudioPlayerのデリゲートをセット
            audioPlayer.delegate = self
            // 音声の再生
            audioPlayer.prepareToPlay()
            audioPlayer.play()
        } catch let error as NSError {
            print(error)
            print("HogeHoge!")
        }
    }
}
